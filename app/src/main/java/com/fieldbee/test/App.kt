package com.fieldbee.test

import android.app.Application
import com.google.android.material.color.DynamicColors

class App : Application() {

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initTheme()
    }

    private fun initTheme() {
        DynamicColors.applyToActivitiesIfAvailable(this)
    }

}