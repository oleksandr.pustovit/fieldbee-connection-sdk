package com.fieldbee.test

import android.Manifest
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresPermission
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.fieldbee.test.notification.NotificationUtils
import com.fieldbee.test.notification.NotificationUtils.ACTION_NOTIFICATION_START
import com.fieldbee.test.notification.NotificationUtils.ACTION_NOTIFICATION_STOP
import com.fieldbee.test.notification.NotificationUtils.NOTIFICATION_ID
import com.fieldbee.fieldbee_sdk.FieldbeeController
import com.fieldbee.fieldbee_sdk.model.DeviceType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class FieldbeeControllerService : Service() {

    private val job = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + job)
    private val mutex = Mutex()

    private val binder = ControllerBinder()

    private val notificationManager: NotificationManagerCompat by lazy {
        NotificationUtils.createNotificationManager(this)
    }
    private val notificationBuilder: NotificationCompat.Builder by lazy {
        NotificationUtils.createNotification(this)
    }

    private val deviceCollection: MutableMap<String, FieldbeeController> = mutableMapOf()
    private val deviceControllerList: List<FieldbeeController> get() = deviceCollection.values.toList()

    @Deprecated("Use method getControllerById()")
    var deviceController: FieldbeeController? = null
        get() = deviceControllerList.firstOrNull()
        private set

    companion object {
        private const val ACTION_START_CONTROLLER = "action_fieldbee.start_controller"
        private const val ARG_FIELDBEE_DEVICE_IP = "arg_fieldbee.device_ip"
        private const val ARG_FIELDBEE_DEVICE_ID = "arg_fieldbee.device_id"
        private const val ARG_FIELDBEE_DEVICE_TYPE = "arg_fieldbee.device_type"

        @SuppressLint("InlinedApi")
        @RequiresPermission(Manifest.permission.POST_NOTIFICATIONS)
        fun getIntentForStartController(
            context: Context,
            deviceIp: String,
            deviceId: String,
            deviceType: DeviceType = DeviceType.POWERSTEER,
        ) = Intent(context, FieldbeeControllerService::class.java).apply {
            action = ACTION_START_CONTROLLER
            putExtra(ARG_FIELDBEE_DEVICE_IP, deviceIp)
            putExtra(ARG_FIELDBEE_DEVICE_ID, deviceId)
            putExtra(ARG_FIELDBEE_DEVICE_TYPE, deviceType.toInt())
        }

        fun startService(
            context: Context,
            intent: Intent = Intent(context, FieldbeeControllerService::class.java),
        ): ComponentName? =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                context.startForegroundService(intent)
            else
                context.startService(intent)

        fun bindService(
            context: Context,
            connection: ServiceConnection,
            intent: Intent = Intent(context, FieldbeeControllerService::class.java),
        ) {
            context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }

        fun startAndBindService(
            context: Context,
            connection: ServiceConnection,
            intent: Intent = Intent(context, FieldbeeControllerService::class.java),
        ) {
            val startIntent = Intent(context, FieldbeeControllerService::class.java)
            ContextCompat.startForegroundService(context, startIntent)
            context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    fun getControllerById(serialNumber: String) = deviceCollection[serialNumber]

    fun addDevice(
        deviceIp: String,
        serialNumber: String,
        deviceType: DeviceType,
    ): FieldbeeController =
        deviceCollection[serialNumber] ?: FieldbeeController(deviceIp, serialNumber, deviceType)
            .also {
                deviceCollection[serialNumber] = it
                observeState()
            }

    fun removeDevice(serialNumber: String) {
        deviceCollection[serialNumber]?.disconnect()
        deviceCollection.remove(serialNumber)
//        notificationManager.cancel(serialNumber, NOTIFICATION_ID)
        observeState()
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        NotificationUtils.getContentText(baseContext, notificationBuilder, deviceControllerList)
            .build()
            .also {
                notificationManager.notify(NOTIFICATION_ID, it)
                startForeground(NOTIFICATION_ID, it)
            }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val serialNumber = intent?.getStringExtra(NotificationUtils.ARG_DEVICE_SERIAL_NUMBER)
        when (intent?.action) {
            ACTION_NOTIFICATION_START -> serialNumber?.let { deviceCollection[it]?.connect() }
            ACTION_NOTIFICATION_STOP -> serialNumber?.let { deviceCollection[it]?.disconnect() }
            ACTION_START_CONTROLLER -> createController(intent)
        }
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder = binder

    override fun onDestroy() {
        coroutineScope.cancel()

        deviceControllerList.forEach { it.disconnect() }
        deviceCollection.clear()

//        notificationManager.cancelAll() // if we have more than one notification
        stopForeground(STOP_FOREGROUND_REMOVE)
    }

    private fun createController(intent: Intent) {
        if (intent.hasExtra(ARG_FIELDBEE_DEVICE_IP) && intent.hasExtra(ARG_FIELDBEE_DEVICE_ID)) {
            val ip = intent.getStringExtra(ARG_FIELDBEE_DEVICE_IP)
            val id = intent.getStringExtra(ARG_FIELDBEE_DEVICE_ID)
            val deviceType = DeviceType.valueOf(
                intent.getIntExtra(ARG_FIELDBEE_DEVICE_TYPE, DeviceType.POWERSTEER.toInt())
            )

            if (!ip.isNullOrBlank() && !id.isNullOrBlank()) {
                if (deviceCollection[id] != null) {
                    deviceCollection[id]?.reconnect(ip, id, deviceType)
                } else {
                    deviceCollection[id] = FieldbeeController(ip, id, deviceType)
                    observeState()
                }
            }
        }
        intent.extras?.clear()
    }

    private fun observeState() {
        if (job.isActive) job.cancelChildren()

        if (deviceControllerList.isEmpty()) {
            updateNotification()
        } else {
            deviceControllerList.forEach { controller ->
                coroutineScope.launch {
                    controller.state.collect {
                        mutex.withLock { updateNotification() }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun updateNotification() {
        notificationBuilder.apply {
            clearActions()
            NotificationUtils.getContentText(baseContext, this, deviceControllerList)

            if (deviceControllerList.size == 1) {
                val controller = deviceControllerList.first()
                NotificationUtils.setActionsToNotification(
                    baseContext, this, controller.deviceId, controller.state.value
                )
            }
        }.also {
            notificationManager.notify(NOTIFICATION_ID, it.build())
//            notificationManager.notify(controller.deviceId, NOTIFICATION_ID, it.build())
        }
    }

    inner class ControllerBinder : Binder() {
        fun getService(): FieldbeeControllerService = this@FieldbeeControllerService
    }

    private fun isServiceRunning(): Boolean =
        (getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?)
            ?.getRunningServices(Int.MAX_VALUE)
            ?.any { this.javaClass.name == it.service.className }
            ?: false

}