package com.fieldbee.test.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.fieldbee.test.FieldbeeControllerService
import com.fieldbee.test.R
import com.fieldbee.fieldbee_sdk.FieldbeeController
import com.fieldbee.fieldbee_sdk.engine.ConnectionState

internal object NotificationUtils {

    private const val CHANNEL_ID = "Fieldbee channel"
    private const val CHANNEL_NAME = "Fieldbee"
    private const val GROUP_KEY_CONNECTION_STATE = "com.fieldbee.device.controller.notification.CONNECTION_STATE"
    private const val BACKGROUND_COLOR = 0xFFCF01

    internal const val NOTIFICATION_ID = 2187

    internal const val ACTION_NOTIFICATION_START = "com.fieldbee.device.controller:Connect"
    internal const val ACTION_NOTIFICATION_STOP = "com.fieldbee.device.controller:Disconnect"

    internal const val ARG_DEVICE_SERIAL_NUMBER = "com.fieldbee.device.controller:SerialNumber"

    fun createNotificationManager(context: Context) = NotificationManagerCompat.from(context)
        .apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.description = context.getString(R.string.fieldbee_controller_channel_description)
                createNotificationChannel(channel)
            }
        }

    fun createNotification(context: Context) = NotificationCompat.Builder(context, CHANNEL_ID)
        .setSmallIcon(R.drawable.ic_notification)
        .setLargeIcon(getBitmapFromVectorDrawable(context, R.drawable.ic_24_fieldbee))
        .setShowWhen(false)
        .setOngoing(true)
        .setLocalOnly(true)
        .setAutoCancel(true)
        .setColor(BACKGROUND_COLOR)
        .setColorized(true)
        .setSilent(true)
        .setChannelId(CHANNEL_ID)
//        .setGroup(GROUP_KEY_CONNECTION_STATE)
        .setCategory(NotificationCompat.CATEGORY_SERVICE)
        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        .setPriority(NotificationManagerCompat.IMPORTANCE_MAX)

    private fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap? {
        val drawable = ContextCompat.getDrawable(context, drawableId)
        val bitmap = Bitmap.createBitmap(
            drawable!!.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun setActionsToNotification(
        context: Context,
        notificationBuilder: NotificationCompat.Builder,
        serialNumber: String,
        state: ConnectionState?
    ): NotificationCompat.Builder = notificationBuilder.apply {
        when (state) {
            ConnectionState.CONNECTED -> addAction(
                R.drawable.ic_stop_24,
                context.getString(R.string.fieldbee_controller_disconnect),
                getPendingIntentStopFieldbee(context, serialNumber)
            )
            ConnectionState.DISCONNECTED -> addAction(
                R.drawable.ic_play_24,
                context.getString(R.string.fieldbee_controller_connect),
                getPendingIntentStartFieldbee(context, serialNumber)
            )
            ConnectionState.CONNECTING -> addAction(
                R.drawable.ic_stop_24,
                context.getString(R.string.fieldbee_controller_disconnect),
                getPendingIntentStopFieldbee(context, serialNumber)
            )
            ConnectionState.IDLE -> addAction(
                R.drawable.ic_play_24,
                context.getString(R.string.fieldbee_controller_connect),
                getPendingIntentStartFieldbee(context, serialNumber)
            )
            ConnectionState.RECONNECTING -> addAction(
                R.drawable.ic_stop_24,
                context.getString(R.string.fieldbee_controller_disconnect),
                getPendingIntentStopFieldbee(context, serialNumber)
            )
            else -> {}
        }
    }

    fun getContentText(
        context: Context,
        notificationBuilder: NotificationCompat.Builder,
        deviceControllerList: List<FieldbeeController>,
    ): NotificationCompat.Builder = notificationBuilder.apply {
        when (deviceControllerList.size) {
            0 -> {
                setContentTitle(context.getString(R.string.fieldbee_controller_no_devices))
                setContentText(null)
                setStyle(null)
            }

            1 -> {
                val controller = deviceControllerList.first()
                setContentTitle(
                    context.getString(
                        R.string.fieldbee_controller_device_simple,
                        controller.deviceId,
                        controller.deviceIp
                    )
                )
                setContentText(
                    context.getString(
                        R.string.fieldbee_controller_state,
                        getStateDescription(context, controller.state.value)
                    )
                )
                setStyle(null)
            }

            else -> {
                setContentTitle(
                    context.getString(
                        R.string.fieldbee_controller_devices,
                        deviceControllerList.size
                    )
                )
                setContentText(null)
                setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(getBigText(context, deviceControllerList))
                        .setBigContentTitle(
                            context.getString(
                                R.string.fieldbee_controller_devices,
                                deviceControllerList.size
                            )
                        )
                )
            }
        }
    }

    private fun getBigText(
        context: Context,
        deviceControllerList: List<FieldbeeController>
    ): String {
        val result = StringBuilder()
        deviceControllerList.forEach { controller ->
            result.appendLine(
                context.getString(
                    R.string.fieldbee_controller_device,
                    controller.deviceId,
                    controller.deviceIp,
                    getStateDescription(context, controller.state.value)
                )
            )
        }
        return result.toString()
    }

    private fun getStateDescription(context: Context, state: ConnectionState): String {
        return context.getString(
            when (state) {
                ConnectionState.IDLE -> R.string.fieldbee_controller_disconnected
                ConnectionState.DISCONNECTING -> R.string.fieldbee_controller_disconnecting
                ConnectionState.DISCONNECTED -> R.string.fieldbee_controller_disconnected
                ConnectionState.CONNECTING -> R.string.fieldbee_controller_connecting
                ConnectionState.CONNECTED -> R.string.fieldbee_controller_connected
                ConnectionState.RECONNECTING -> R.string.fieldbee_controller_reconnecting
            }
        )
    }

    private fun getPendingIntentStartFieldbee(
        context: Context,
        serialNumber: String,
    ): PendingIntent {
        val intent = Intent(context, FieldbeeControllerService::class.java)
        intent.action = ACTION_NOTIFICATION_START
        intent.putExtra(ARG_DEVICE_SERIAL_NUMBER, serialNumber)
        return PendingIntent.getService(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun getPendingIntentStopFieldbee(
        context: Context,
        serialNumber: String,
    ): PendingIntent {
        val intent = Intent(context, FieldbeeControllerService::class.java)
        intent.action = ACTION_NOTIFICATION_STOP
        intent.putExtra(ARG_DEVICE_SERIAL_NUMBER, serialNumber)
        return PendingIntent.getService(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

}