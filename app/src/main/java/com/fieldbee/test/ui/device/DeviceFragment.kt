package com.fieldbee.test.ui.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentDeviceBinding
import com.fieldbee.test.utils.PermissionStatus
import com.fieldbee.test.utils.requestPermissionLauncher
import com.fieldbee.fieldbee_sdk.model.DeviceType
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice

class DeviceFragment : Fragment(R.layout.fragment_device) {

    private val viewModel: DeviceViewModel by viewModels {
        DeviceViewModelFactory(requireActivity().application, device!!)
    }

    private var _binding: FragmentDeviceBinding? = null
    private val binding get() = _binding!!

    private val device: FieldbeeDevice? by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requireArguments().getSerializable(ARG_FIELDBEE_DEVICE, FieldbeeDevice::class.java)
        } else {
            requireArguments().getSerializable(ARG_FIELDBEE_DEVICE) as FieldbeeDevice?
        }
    }

    private val notificationPermissionsLauncher by requestPermissionLauncher { status ->
        when (status) {
            PermissionStatus.Granted -> viewModel.bindToService()
            PermissionStatus.Denied -> viewModel.bindToService()
            PermissionStatus.ShowRationale -> viewModel.bindToService()
        }
    }

    private val storagePermissionsLauncher by requestPermissionLauncher { status ->
        when (status) {
            PermissionStatus.Granted -> {
                Toast.makeText(
                    requireContext(),"PermissionStatus granted. Try save again", Toast.LENGTH_LONG
                ).show()
            }

            PermissionStatus.Denied -> {
                Toast.makeText(requireContext(), "PermissionStatus denied", Toast.LENGTH_LONG)
                    .show()
            }

            PermissionStatus.ShowRationale -> {
                Toast.makeText(requireContext(), "PermissionStatus denied", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    companion object {
        private const val ARG_FIELDBEE_DEVICE = "arg_fieldbee_device"

        fun getArguments(device: FieldbeeDevice) = Bundle().apply {
            putSerializable(ARG_FIELDBEE_DEVICE, device)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
        initToolbarMenu()
        setListeners()
        setObservables()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView(view: View) {
        _binding = FragmentDeviceBinding.bind(view)
        binding.tvLogs.movementMethod = ScrollingMovementMethod()
        binding.etDeviceIp.setText(device?.deviceIp ?: "0.0.0.0")
    }

    private fun initToolbarMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_device, menu)
            }

            @SuppressLint("MissingPermission")
            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.menu_save_logs -> {
                        if (checkStoragePermissions()) viewModel.saveDeviceLogFiles()
                        true
                    }
                    R.id.menu_all_save_logs -> {
                        if (checkStoragePermissions()) viewModel.saveAllLogsFiles()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setListeners() {
        binding.apply {
            btnConnect.setOnClickListener {
                viewModel.connect()
            }
            btnDisconnect.setOnClickListener {
                viewModel.disconnect()
            }
            btnPrintLog.setOnClickListener {
                viewModel.printLog()
            }
            btnParameters.setOnClickListener {
                viewModel.listenParameters()
            }
            btnConnectAsb.setOnClickListener {
                viewModel.reconnectToDevice("192.168.13.62", "FB000053", DeviceType.POWERSTEER)
            }
            btnConnectBase.setOnClickListener {
                viewModel.reconnectToDevice("192.168.13.69", "0D3C370E", DeviceType.BASE)
            }
            btnReconnect.setOnClickListener {
                viewModel.reconnectToDevice(binding.etDeviceIp.text.toString(), device!!.deviceId, device!!.type)
            }
            btnFuzzy.setOnClickListener {
                viewModel.getFuzzyParams()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setObservables() {
        viewModel.apply {
            message.observe(viewLifecycleOwner) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
            startScanFlag.observe(viewLifecycleOwner) {
                checkNotificationPermissions()
            }
            log.observe(viewLifecycleOwner) {
                binding.tvLogs.append(it)
            }
            connectionState.observe(viewLifecycleOwner) {
                binding.tvStateValue.text = it.toString()
            }
            exception.observe(viewLifecycleOwner) {
                binding.tvExceptionValue.text = it
            }
            nmea.observe(viewLifecycleOwner) {
                binding.tvNmea.text = "NMEA:\n$it"
            }
            position.observe(viewLifecycleOwner) {
                binding.tvPosition.text = "Position:\n$it"
            }
            tiltedPosition.observe(viewLifecycleOwner) {
                binding.tvTiltedPosition.text = "Tilted Position:\n$it"
            }
            positioningAccuracy.observe(viewLifecycleOwner) {
                binding.tvPositioningAccuracy.text = "Positioning Accuracy:\n$it"
            }
            deviceStatus.observe(viewLifecycleOwner) {
                binding.tvDeviceStatus.text = "Device Status:\n$it"
            }
            deviceImu.observe(viewLifecycleOwner) {
                binding.tvDeviceImu.text = "Device Imu:\n$it"
            }
            steeringState.observe(viewLifecycleOwner) {
                binding.tvSteeringState.text = "Steering State:\n$it"
            }
            sectionControl.observe(viewLifecycleOwner) {
                binding.tvSectionControl.text = "Section Control:\n$it"
            }
            ntripInfo.observe(viewLifecycleOwner) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkNotificationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
            && ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            notificationPermissionsLauncher.launch(arrayOf(Manifest.permission.POST_NOTIFICATIONS))
        } else {
            viewModel.bindToService()
        }
    }

    private fun checkStoragePermissions(): Boolean {
        return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P
            && ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            storagePermissionsLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            false
        } else {
            true
        }
    }

}