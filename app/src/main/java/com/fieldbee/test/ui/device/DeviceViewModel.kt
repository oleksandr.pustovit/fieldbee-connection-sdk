package com.fieldbee.test.ui.device

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.annotation.RequiresPermission
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.fieldbee.fieldbee_sdk.FieldbeeController
import com.fieldbee.fieldbee_sdk.data.repository.SteeringAdjustmentRepository
import com.fieldbee.fieldbee_sdk.data.repository.factory.RepositoryFactory
import com.fieldbee.fieldbee_sdk.engine.ConnectionState
import com.fieldbee.fieldbee_sdk.model.DeviceType
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice
import com.fieldbee.file_logger.FieldBeeLogger
import com.fieldbee.test.FieldbeeControllerService
import com.fieldbee.test.utils.SingleEventLiveData
import kotlinx.coroutines.launch

class DeviceViewModel(
    application: Application,
    private val fieldbeeDevice: FieldbeeDevice
) : AndroidViewModel(application) {

    private val context = getApplication<Application>()

    @SuppressLint("StaticFieldLeak")
    private var service: FieldbeeControllerService? = null
    private var deviceController: FieldbeeController? = null

    private var steeringRepository: SteeringAdjustmentRepository? = null

    private var isServiceBind = false

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, iBinder: IBinder) {
            val binder = iBinder as FieldbeeControllerService.ControllerBinder
            service = binder.getService()
            isServiceBind = true
            deviceController = binder.getService().deviceController
            deviceController?.initNmeaLogger()

            deviceController?.let {
                steeringRepository = RepositoryFactory.create(it.deviceIp, it.deviceType)
            }

            deviceConnected()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isServiceBind = false
            service = null
            deviceController = null
        }
    }

    private val _nmea = MutableLiveData<String>()
    val nmea: LiveData<String> = _nmea

    private val _log = MutableLiveData<String>()
    val log: LiveData<String> = _log

    private val _position = MutableLiveData<String>()
    val position: LiveData<String> = _position

    private val _tiltedPosition = MutableLiveData<String>()
    val tiltedPosition: LiveData<String> = _tiltedPosition

    private val _positioningAccuracy = MutableLiveData<String>()
    val positioningAccuracy: LiveData<String> = _positioningAccuracy

    private val _deviceStatus = MutableLiveData<String>()
    val deviceStatus: LiveData<String> = _deviceStatus

    private val _deviceImu = MutableLiveData<String>()
    val deviceImu: LiveData<String> = _deviceImu

    private val _steeringState = MutableLiveData<String>()
    val steeringState: LiveData<String> = _steeringState

    private val _sectionControl = MutableLiveData<String>()
    val sectionControl: LiveData<String> = _sectionControl

    private val _message = SingleEventLiveData<String>()
    val message: LiveData<String> = _message

    private val _connectionState = MutableLiveData(ConnectionState.DISCONNECTED)
    val connectionState: LiveData<ConnectionState> = _connectionState

    private val _exception = MutableLiveData<String>()
    val exception: LiveData<String> = _exception

    private val _startScanFlag = SingleEventLiveData<Boolean>()
    val startScanFlag: LiveData<Boolean> = _startScanFlag

    private val _ntripInfo = SingleEventLiveData<String>()
    val ntripInfo: LiveData<String> = _ntripInfo

    init {
        _startScanFlag.value = true
    }

    @SuppressLint("MissingPermission")
    fun bindToService() {
        FieldbeeControllerService.getIntentForStartController(
            context,
            fieldbeeDevice.deviceIp,
            fieldbeeDevice.deviceId
        ).also { intent ->
            context.startService(intent)?.let {
                context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun reconnectToDevice(deviceIp: String, deviceId: String, deviceType: DeviceType) {
        deviceController?.reconnect(deviceIp, deviceId, deviceType)
    }

    private fun deviceConnected() {
        viewModelScope.launch {
            deviceController?.state?.collect {
                _connectionState.postValue(it)
                if (it == ConnectionState.CONNECTED) _exception.postValue("NONE")
            }
        }

        viewModelScope.launch {
            deviceController?.throwable?.collect {
                _exception.postValue(it.message)
            }
        }

        viewModelScope.launch {
            deviceController?.getNmeaRawFlow()?.collect {
                _nmea.postValue(it)
            }
        }
        connect()
    }

    fun connect() {
        deviceController?.connect()
    }

    fun disconnect() {
        deviceController?.disconnect()
    }

    fun printLog() {
        viewModelScope.launch {
            deviceController?.getNmeaRawFlow()?.collect {
                _log.value = "$it\n"
            }
        }
    }

    fun listenParameters() {
        viewModelScope.launch {
            deviceController?.getPositionFlow()?.collect {
                _position.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getTiltedPositionFlow()?.collect {
                _tiltedPosition.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getPositioningAccuracyFlow()?.collect {
                _positioningAccuracy.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getDeviceStatusFlow()?.collect {
                _deviceStatus.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getDeviceImuFlow()?.collect {
                _deviceImu.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getSteeringStateFlow()?.collect {
                _steeringState.value = it.toString()
            }
        }
        viewModelScope.launch {
            deviceController?.getSectionControlFlow()?.collect {
                _sectionControl.value = it.toString()
            }
        }
    }

    fun getFuzzyParams() {
        viewModelScope.launch {
            steeringRepository?.getFuzzy()
                ?.onSuccess {
                    _ntripInfo.value = """
                           FUZZY:
                        dFar:   ${it.dFar}
                        dNear:  ${it.dNear}
                        dZero:  ${it.dZero}
                    """.trimIndent()
                }?.onFailure {
                    _ntripInfo.value = "Something went wrong $it"
                }
        }
    }

    @RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun saveDeviceLogFiles() {
        val deviceId = fieldbeeDevice.deviceId
        FieldBeeLogger.getInstance(deviceId).saveDeviceLogsToExternalStorage()
    }

    @RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun saveAllLogsFiles() {
        FieldBeeLogger.saveAllLogsToExternalStorage()
    }

    override fun onCleared() {
        context.unbindService(serviceConnection)
        context.stopService(Intent(context, FieldbeeControllerService::class.java))
    }

}