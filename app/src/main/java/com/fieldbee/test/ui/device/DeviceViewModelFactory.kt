package com.fieldbee.test.ui.device

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice

class DeviceViewModelFactory(
    private val application: Application,
    private val fieldbeeDevice: FieldbeeDevice,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass == DeviceViewModel::class.java) {
            @Suppress("UNCHECKED_CAST")
            DeviceViewModel(application, fieldbeeDevice) as T
        } else {
            throw IllegalArgumentException()
        }
    }

}