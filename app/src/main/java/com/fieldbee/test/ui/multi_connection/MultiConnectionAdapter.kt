package com.fieldbee.test.ui.multi_connection

import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldbee.test.databinding.FragmentMultiConnectionItemBinding

class MultiConnectionAdapter :
    ListAdapter<MultiConnectionItemUiState, MultiConnectionAdapter.ViewHolder>(ItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            FragmentMultiConnectionItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemState = getItem(position)
        with(holder.binding) {
            ivDeviceType.setImageResource(itemState.icon)
            ivDeviceType.setColorFilter(
                ContextCompat.getColor(ivDeviceType.context, itemState.iconColor),
                PorterDuff.Mode.SRC_IN
            )
            tvDeviceId.text = itemState.deviceId
            tvDeviceIp.text = itemState.deviceIp
            tvDeviceConnectionStatus.setText(itemState.status)
            tvNmeaValue.text = itemState.currentNmeaLine
            progressIndicator.visibility = itemState.progressVisibility
            tvConnectionException.text = itemState.exception
            btnConnectAction.visibility = itemState.buttonVisibility
            btnConnectAction.setText(itemState.btnConnectionText)
            btnConnectAction.setOnClickListener {
                itemState.btnConnectionAction.invoke()
            }
            btnRemove.setOnClickListener {
                itemState.btnRemoveAction.invoke(itemState.deviceId)
            }
        }
    }

    inner class ViewHolder(
        val binding: FragmentMultiConnectionItemBinding,
    ) : RecyclerView.ViewHolder(binding.root)

    object ItemCallback : DiffUtil.ItemCallback<MultiConnectionItemUiState>() {
        override fun areItemsTheSame(
            oldItem: MultiConnectionItemUiState,
            newItem: MultiConnectionItemUiState,
        ): Boolean {
            return oldItem.deviceId == newItem.deviceId
        }

        override fun areContentsTheSame(
            oldItem: MultiConnectionItemUiState,
            newItem: MultiConnectionItemUiState,
        ): Boolean {
            return oldItem == newItem
        }
    }

}