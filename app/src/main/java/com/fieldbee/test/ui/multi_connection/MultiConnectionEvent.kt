package com.fieldbee.test.ui.multi_connection

import java.util.*

sealed class MultiConnectionEvent {
    val uniqueId: Long = UUID.randomUUID().mostSignificantBits

    object PermissionRequest : MultiConnectionEvent()
}