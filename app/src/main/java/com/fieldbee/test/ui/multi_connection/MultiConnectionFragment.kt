package com.fieldbee.test.ui.multi_connection

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentMultiConnectionBinding
import com.fieldbee.test.utils.PermissionStatus
import com.fieldbee.test.utils.SpaceItemDecoration
import com.fieldbee.test.utils.requestPermissionLauncher
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class MultiConnectionFragment : Fragment(R.layout.fragment_multi_connection) {

    private val viewModel: MultiConnectionViewModel by viewModels()

    private var _binding: FragmentMultiConnectionBinding? = null
    private val binding get() = _binding!!

    private val adapter = MultiConnectionAdapter()

    private val notificationPermissionsLauncher by requestPermissionLauncher { status ->
        when (status) {
            PermissionStatus.Granted -> viewModel.bindService()
            PermissionStatus.Denied -> viewModel.bindService()
            PermissionStatus.ShowRationale -> viewModel.bindService()
        }
    }

    private val storagePermissionsLauncher by requestPermissionLauncher { status ->
        when (status) {
            PermissionStatus.Granted -> {
                Toast.makeText(
                    requireContext(), "PermissionStatus granted. Try save again", Toast.LENGTH_LONG
                ).show()
            }

            PermissionStatus.Denied -> {
                Toast.makeText(requireContext(), "PermissionStatus denied", Toast.LENGTH_LONG)
                    .show()
            }

            PermissionStatus.ShowRationale -> {
                Toast.makeText(requireContext(), "PermissionStatus denied", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    companion object {
        private const val ARG_REQUEST_FIELDBEE_DEVICE = "request_arg.device"
        const val REQUEST_KEY_FIELDBEE_DEVICE = "request_key.device_ip"

        fun getBundleForResult(device: FieldbeeDevice) = Bundle().apply {
            putSerializable(ARG_REQUEST_FIELDBEE_DEVICE, device)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
        initToolbarMenu()
        setListeners()
        setObservables()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView(view: View) {
        _binding = FragmentMultiConnectionBinding.bind(view)

        binding.recyclerView.apply {
            adapter = this@MultiConnectionFragment.adapter
            setHasFixedSize(true)
            addItemDecoration(SpaceItemDecoration(16))
        }
    }

    private fun initToolbarMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_multi_connection, menu)
            }

            @SuppressLint("MissingPermission")
            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.menu_all_save_logs -> {
                        if (checkStoragePermissions()) viewModel.saveAllLogsFiles()
                        true
                    }

                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setListeners() {
        setFragmentResultListener(REQUEST_KEY_FIELDBEE_DEVICE) { _, bundle ->
            val device: FieldbeeDevice? =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    bundle.getSerializable(ARG_REQUEST_FIELDBEE_DEVICE, FieldbeeDevice::class.java)
                } else {
                    bundle.getSerializable(ARG_REQUEST_FIELDBEE_DEVICE) as FieldbeeDevice?
                }
            device?.let {
                viewModel.addDevice(device)
                Toast.makeText(requireContext(), device.toString(), Toast.LENGTH_LONG).show()
            }
        }

        binding.apply {
            fab.setOnClickListener {
                findNavController().navigate(R.id.action_multiConnectionFragment_to_simpleSearchFragment)
            }
        }
    }

    private fun setObservables() {
        viewModel.startScanFlag.observe(viewLifecycleOwner) {
            checkNotificationPermissions()
        }
        viewModel.uiState
            .onEach { adapter.submitList(it) }
            .flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun checkNotificationPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU
            && ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            notificationPermissionsLauncher.launch(arrayOf(Manifest.permission.POST_NOTIFICATIONS))
        } else {
            viewModel.bindService()
        }
    }

    private fun checkStoragePermissions(): Boolean {
        return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P
            && ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            storagePermissionsLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            false
        } else {
            true
        }
    }

}