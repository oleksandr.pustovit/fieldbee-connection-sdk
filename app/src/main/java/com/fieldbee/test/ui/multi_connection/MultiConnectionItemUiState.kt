package com.fieldbee.test.ui.multi_connection

import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.fieldbee.test.R

data class MultiConnectionItemUiState(
    val deviceId: String,
    val deviceIp: String,
    val currentNmeaLine: String = "...",
    @DrawableRes val icon: Int,
    @ColorRes val iconColor: Int = R.color.yellow,
    @StringRes val status: Int = R.string.connection_status_disconnected,
    @StringRes val btnConnectionText: Int = R.string.empty_string,
    val btnConnectionAction: () -> Unit = {},
    val btnRemoveAction: (serialNumber: String) -> Unit = {},
    val exception: String = "",
    val buttonVisibility: Int = View.GONE,
    val progressVisibility: Int = View.VISIBLE,
)