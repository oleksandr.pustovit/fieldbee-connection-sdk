package com.fieldbee.test.ui.multi_connection

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.view.View
import androidx.annotation.RequiresPermission
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.fieldbee.test.FieldbeeControllerService
import com.fieldbee.test.R
import com.fieldbee.test.utils.SingleEventLiveData
import com.fieldbee.fieldbee_sdk.FieldbeeController
import com.fieldbee.fieldbee_sdk.engine.ConnectionState
import com.fieldbee.fieldbee_sdk.model.DeviceType
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice
import com.fieldbee.file_logger.FieldBeeLogger
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class MultiConnectionViewModel(application: Application) : AndroidViewModel(application) {

    private val context = getApplication<Application>()
    private val mutex = Mutex()

    @SuppressLint("StaticFieldLeak")
    private var service: FieldbeeControllerService? = null

    private var isServiceBind = false

    private val _uiState = MutableStateFlow(listOf<MultiConnectionItemUiState>())
    val uiState: StateFlow<List<MultiConnectionItemUiState>> = _uiState.asStateFlow()

    private val _startScanFlag = SingleEventLiveData<Boolean>()
    val startScanFlag: LiveData<Boolean> = _startScanFlag

    private val deviceMap: MutableMap<String, MultiConnectionItemUiState> = mutableMapOf()


    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, iBinder: IBinder) {
            val binder = iBinder as FieldbeeControllerService.ControllerBinder
            service = binder.getService()
            isServiceBind = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            isServiceBind = false
            service = null
        }
    }

    init {
        _startScanFlag.value = true
    }

    @SuppressLint("MissingPermission")
    fun bindService() {
        FieldbeeControllerService.startAndBindService(context, serviceConnection)
    }

    fun addDevice(device: FieldbeeDevice) {
        service?.addDevice(device.deviceIp, device.deviceId, device.type)?.let { deviceController ->
            val state = MultiConnectionItemUiState(
                deviceId = device.deviceId,
                deviceIp = device.deviceIp,
                icon = when (device.type) {
                    DeviceType.BASE -> R.drawable.ic_24_fieldbee_l2_base
                    DeviceType.ROVER -> R.drawable.ic_24_fieldbee_l2_rover
                    DeviceType.POWERSTEER -> R.drawable.ic_24_fieldbee_asb_powersteer
                    DeviceType.MOVING_BASE -> R.drawable.ic_24_fieldbee_asb_moving_base
                    DeviceType.MOVING_ROVER -> R.drawable.ic_24_fieldbee_asb_moving_rover
                },
            )
            deviceMap[state.deviceId] = state
            updateStates()

            deviceController.initNmeaLogger()
            deviceConnected(deviceController)
        }
    }

    private fun removeDeviceById(serialNumber: String) {
        viewModelScope.launch {
            mutex.withLock {
                service?.removeDevice(serialNumber)
                deviceMap.remove(serialNumber)
                updateStates()
            }
        }
    }

    private fun deviceConnected(deviceController: FieldbeeController) {
        viewModelScope.launch {
            deviceController.state.collect { connectionState ->
                deviceMap[deviceController.deviceId]?.let { connectionItem ->
                    val newItem = handleConnectionState(
                        connectionItem.copy(
                            btnRemoveAction = { removeDeviceById(it) }
                        ),
                        deviceController,
                        connectionState)
                    deviceMap.replace(deviceController.deviceId, newItem)
                    updateStates()
                }
            }
        }

        viewModelScope.launch {
            deviceController.throwable.collect {
                deviceMap[deviceController.deviceId]?.let { oldState ->
                    val newItem = oldState.copy(exception = it.message.toString())
                    deviceMap.replace(deviceController.deviceId, newItem)
                    updateStates()
                }
            }
        }

        viewModelScope.launch {
            deviceController.getNmeaRawFlow().collect {
                deviceMap[deviceController.deviceId]?.let { oldState ->
                    val newItem = oldState.copy(currentNmeaLine = it)
                    deviceMap.replace(deviceController.deviceId, newItem)
                    updateStates()
                }
            }
        }
        deviceController.connect()
    }

    private fun handleConnectionState(
        state: MultiConnectionItemUiState,
        deviceController: FieldbeeController,
        connectionState: ConnectionState,
    ) = when (connectionState) {
        ConnectionState.IDLE -> state.copy(
            iconColor = R.color.yellow,
            status = R.string.connection_status_disconnected,
            btnConnectionText = R.string.connect,
            btnConnectionAction = { deviceController.connect() },
            buttonVisibility = View.VISIBLE,
            progressVisibility = View.GONE,
            exception = ""
        )

        ConnectionState.CONNECTED -> state.copy(
            iconColor = R.color.green,
            status = R.string.connection_status_connected,
            btnConnectionText = R.string.disconnect,
            btnConnectionAction = { deviceController.disconnect() },
            buttonVisibility = View.VISIBLE,
            progressVisibility = View.GONE,
            exception = ""
        )

        ConnectionState.CONNECTING -> state.copy(
            iconColor = R.color.yellow,
            status = R.string.connection_status_connecting,
            btnConnectionText = R.string.disconnect,
            btnConnectionAction = { deviceController.disconnect() },
            buttonVisibility = View.VISIBLE,
            progressVisibility = View.VISIBLE,
        )

        ConnectionState.DISCONNECTED -> state.copy(
            iconColor = R.color.red,
            status = R.string.connection_status_disconnected,
            btnConnectionText = R.string.connect,
            btnConnectionAction = { deviceController.connect() },
            buttonVisibility = View.VISIBLE,
            progressVisibility = View.GONE
        )

        ConnectionState.DISCONNECTING -> state.copy(
            iconColor = R.color.red,
            status = R.string.connection_status_disconnecting,
            btnConnectionText = R.string.empty_string,
            btnConnectionAction = {},
            buttonVisibility = View.GONE,
            progressVisibility = View.VISIBLE
        )

        ConnectionState.RECONNECTING -> state.copy(
            iconColor = R.color.yellow,
            status = R.string.connection_status_reconnecting,
            btnConnectionText = R.string.disconnect,
            btnConnectionAction = { deviceController.disconnect() },
            buttonVisibility = View.VISIBLE,
            progressVisibility = View.VISIBLE,
        )
    }

    @RequiresPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun saveAllLogsFiles() {
        FieldBeeLogger.saveAllLogsToExternalStorage()
    }

    private fun updateStates() {
        viewModelScope.launch {
            mutex.withLock {
                _uiState.value = deviceMap.values.toList()
            }
        }
    }

    override fun onCleared() {
        context.unbindService(serviceConnection)
        context.stopService(Intent(context, FieldbeeControllerService::class.java))
    }

}