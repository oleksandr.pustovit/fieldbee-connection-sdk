package com.fieldbee.test.ui.search

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fieldbee.test.databinding.FragmentSearchDeviceItemBinding
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice

class SearchDeviceAdapter : RecyclerView.Adapter<SearchDeviceAdapter.ViewHolder>() {

    private val items = mutableListOf<FieldbeeDevice>()

    private var onItemClickListener: ((item: FieldbeeDevice) -> Unit)? = null

    private var onItemLongClickListener: ((item: FieldbeeDevice) -> Unit)? = null

    fun setOnItemClickListener(listener: (item: FieldbeeDevice) -> Unit) {
        onItemClickListener = listener
    }

    fun setOnItemLongClickListener(listener: (item: FieldbeeDevice) -> Unit) {
        onItemLongClickListener = listener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: List<FieldbeeDevice>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            FragmentSearchDeviceItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        ).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemClickListener?.invoke(items[adapterPosition])
                }
            }
            itemView.setOnLongClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemLongClickListener?.invoke(items[adapterPosition])
                }
                true
            }
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(
        private val binding: FragmentSearchDeviceItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val item = items[adapterPosition]
            binding.apply {
                tvDeviceId.text = item.deviceId
                tvDeviceIp.text = item.deviceIp
            }
        }
    }
}