package com.fieldbee.test.ui.search

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentSearchDeviceBinding
import com.fieldbee.test.ui.device.DeviceFragment

class SearchDeviceFragment : Fragment(R.layout.fragment_search_device) {

    private val viewModel: SearchDeviceViewModel by viewModels()

    private var _binding: FragmentSearchDeviceBinding? = null
    private val binding get() = _binding!!

    private val adapter = SearchDeviceAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
        setListeners()
        setObservables()
        lock()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView(view: View) {
        _binding = FragmentSearchDeviceBinding.bind(view)

        binding.recyclerView.apply {
            adapter = this@SearchDeviceFragment.adapter
            setHasFixedSize(true)
        }
    }

    private fun setListeners() {
        adapter.apply {
            setOnItemClickListener {
                viewModel.stopSearch()
                findNavController().navigate(
                    R.id.action_searchDeviceFragment_to_deviceFragment,
                    DeviceFragment.getArguments(it)
                )
            }
        }
        binding.apply {
            btnSearchDevice.setOnClickListener {
                viewModel.startScan()
            }
            btnStop.setOnClickListener {
                viewModel.stopSearch()
            }
        }
    }

    private fun setObservables() {
        viewModel.apply {
            progress.observe(viewLifecycleOwner) { isProgressVisible ->
                if (isProgressVisible) binding.progressIndicator.show()
                else binding.progressIndicator.hide()
            }
            deviceList.observe(viewLifecycleOwner) { devices ->
                adapter.setItems(devices)
            }
            stopScanningProgress.observe(viewLifecycleOwner) {
                binding.apply {
                    if (it) {
                        btnSearchDevice.isEnabled = false
                        btnStop.isEnabled = false
                        progressIndicatorStopScan.show()
                    } else {
                        btnSearchDevice.isEnabled = true
                        btnStop.isEnabled = true
                        progressIndicatorStopScan.hide()
                    }
                }
            }
        }
    }

    private fun lock() {
        (requireContext().getSystemService(Context.WIFI_SERVICE) as WifiManager)
            .createMulticastLock("lock")
            .apply {
                setReferenceCounted(true)
                acquire()
            }
    }

}