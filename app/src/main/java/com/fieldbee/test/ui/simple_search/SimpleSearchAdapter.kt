package com.fieldbee.test.ui.simple_search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentSimpleSearchItemBinding
import com.fieldbee.fieldbee_sdk.model.DeviceType
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice

class SimpleSearchAdapter :
    ListAdapter<FieldbeeDevice, SimpleSearchAdapter.ViewHolder>(ItemCallback) {

    private var onItemClickListener: ((item: FieldbeeDevice) -> Unit)? = null

    fun setOnItemClickListener(listener: (item: FieldbeeDevice) -> Unit) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            FragmentSimpleSearchItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        ).apply {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemClickListener?.invoke(getItem(adapterPosition))
                }
            }
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val device = getItem(position)
        with(holder.binding) {
            tvDeviceId.text = device.deviceId
            tvDeviceIp.text = device.deviceIp
            ivDeviceType.setImageResource(
                when (device.type) {
                    DeviceType.BASE -> R.drawable.ic_24_fieldbee_l2_base
                    DeviceType.ROVER -> R.drawable.ic_24_fieldbee_l2_rover
                    DeviceType.POWERSTEER -> R.drawable.ic_24_fieldbee_asb_powersteer
                    DeviceType.MOVING_BASE -> R.drawable.ic_24_fieldbee_asb_moving_base
                    DeviceType.MOVING_ROVER -> R.drawable.ic_24_fieldbee_asb_moving_rover
                }
            )
        }
    }

    inner class ViewHolder(val binding: FragmentSimpleSearchItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    object ItemCallback : DiffUtil.ItemCallback<FieldbeeDevice>() {
        override fun areItemsTheSame(oldItem: FieldbeeDevice, newItem: FieldbeeDevice): Boolean {
            return oldItem.deviceId == newItem.deviceId
        }

        override fun areContentsTheSame(oldItem: FieldbeeDevice, newItem: FieldbeeDevice): Boolean {
            return oldItem == newItem
        }
    }

}