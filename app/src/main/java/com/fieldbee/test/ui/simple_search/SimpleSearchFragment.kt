package com.fieldbee.test.ui.simple_search

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentSimpleSearchBinding
import com.fieldbee.test.ui.multi_connection.MultiConnectionFragment
import com.fieldbee.test.utils.SpaceItemDecoration

class SimpleSearchFragment : Fragment(R.layout.fragment_simple_search) {

    private val viewModel: SimpleSearchViewModel by viewModels()

    private var _binding: FragmentSimpleSearchBinding? = null
    private val binding get() = _binding!!

    private val adapter = SimpleSearchAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
        setListeners()
        setObservables()
        lock()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView(view: View) {
        _binding = FragmentSimpleSearchBinding.bind(view)

        binding.recyclerView.apply {
            adapter = this@SimpleSearchFragment.adapter
            setHasFixedSize(true)
            addItemDecoration(SpaceItemDecoration(16))
        }
    }

    private fun setListeners() {
        adapter.apply {
            setOnItemClickListener { fieldbeeDevice ->
                viewModel.stopSearch()
                setFragmentResult(
                    MultiConnectionFragment.REQUEST_KEY_FIELDBEE_DEVICE,
                    MultiConnectionFragment.getBundleForResult(fieldbeeDevice)
                )
                findNavController().popBackStack()
            }
        }
        binding.apply {

        }
    }

    private fun setObservables() {
        viewModel.apply {
            progress.observe(viewLifecycleOwner) { isProgressVisible ->
                if (isProgressVisible) binding.progressIndicator.show()
                else binding.progressIndicator.hide()
            }
            deviceList.observe(viewLifecycleOwner) { devices ->
                adapter.submitList(devices)
            }
        }
    }

    private fun lock() {
        (requireContext().getSystemService(Context.WIFI_SERVICE) as WifiManager)
            .createMulticastLock("lock")
            .apply {
                setReferenceCounted(true)
                acquire()
            }
    }

}