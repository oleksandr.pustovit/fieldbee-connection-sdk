package com.fieldbee.test.ui.simple_search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fieldbee.fieldbee_sdk.FieldbeeScanner
import com.fieldbee.fieldbee_sdk.model.DeviceType
import com.fieldbee.fieldbee_sdk.model.FieldbeeDevice
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.launch

class SimpleSearchViewModel : ViewModel() {

    private val fieldbeeScanner = FieldbeeScanner(*DeviceType.values())

    private val _progress = MutableLiveData(false)
    val progress: LiveData<Boolean> = _progress

    private val _deviceList = MutableLiveData<List<FieldbeeDevice>>()
    val deviceList: LiveData<List<FieldbeeDevice>> = _deviceList

    init {
        startScan()
    }

    private fun startScan() {
        _progress.value = true
        viewModelScope.launch {
            fieldbeeScanner.startScanFlow()
                .onCompletion {
                    _progress.value = false
                }.collect {
                    _deviceList.value = it
                }
        }
    }

    fun stopSearch() {
        if (progress.value == true) {
            _progress.value = false
            fieldbeeScanner.stopScan()
        }
    }

}