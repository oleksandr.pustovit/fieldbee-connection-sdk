package com.fieldbee.test.ui.welcome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fieldbee.test.R
import com.fieldbee.test.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment(R.layout.fragment_welcome) {

    private var _binding: FragmentWelcomeBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView(view: View) {
        _binding = FragmentWelcomeBinding.bind(view)
    }

    private fun setListeners() {
        binding.apply {
            btnSingleConnection.setOnClickListener {
                findNavController().navigate(R.id.action_welcomeFragment_to_searchDeviceFragment)
            }
            btnMultiConnection.setOnClickListener {
                findNavController().navigate(R.id.action_welcomeFragment_to_multiConnectionFragment)
            }
        }
    }

}